extern crate chrono;

use std::io;
use std::io::Write;
use std::fs::OpenOptions;
use structopt::StructOpt;
use chrono::Local;
use std::path::PathBuf;

use std::fs;


#[derive(StructOpt)]
struct Cli {

/*
	#[structopt(short = "p", long = "path", default_value="./data",  help = "The sets the base name for the path. PANAS will for example be aded for the PANAS question data. Also assumes .txt. So do not enter that")]//,parse(from_os_str))]
	path: String,
*/
	#[structopt(short = "sh", long = "show",help = "Should your scores be shown")]
	show_scores: bool

}

struct Feeling {
	name: String,
	value: u32
}


fn read_happiness(words: Vec<&str>) -> Vec<Feeling>{

	let mut values: Vec<Feeling> = Vec::new();

	for word in words {

		let mut stop = true;

		while stop{

			println!("{}:", word);

			let mut input_line = String::new();
			io::stdin()
			.read_line(&mut input_line)
			.expect("Failed to read input");

			let trimmed_input = input_line.trim();

			match trimmed_input.parse::<u32>() {
				Ok(i) => {
					if i <= 5 {
						values.push(
							Feeling{
								name: word.to_string(),
								value: i
							}
						);
						stop = false;
					} else {
						println!("Please input an integer between 1-5");
						stop = true;
					}
				},
				Err(..) => {
					println!("Please input an integer");
				 	stop = true
				}
			};

		}


	}
	values
}


fn score_calculator(value_words: Vec<&str>,input_data: &Vec<Feeling> ) -> u32 {
	let mut score = 0;

	for word in value_words {
		for value in input_data {
			if value.name == word {
				score += value.value;
			}
		}
	}
	score
}


fn write_values_to_file(path: &std::path::PathBuf, data:&Vec<Feeling> ){


	let mut file = OpenOptions::new()
	.read(true)
	.write(true)
	.append(true)
	.create(true)
	.open(&path)
	.unwrap();



	let date = Local::now();
	let mut write_string = String::from(date.format("%Y:%m:%d").to_string() + ",");

	for word in data {
		write_string += &(word.value.to_string() + ",");
	}

	file.write((write_string +"\n").as_bytes())
	.expect("Failed to write to file");

}


fn read_life_question(questions: Vec<&str>) -> Vec<u8>{

	let mut question_data: Vec<u8>= Vec::new();

	for word in questions{

		let mut con = true;

		while con{
			println!("Did you {} today?", word);
			let mut input_line = String::new();
			io::stdin()
			.read_line(&mut input_line)
			.expect("Failed to read input");

			let trimmed_input = input_line.trim();


			match trimmed_input.parse::<u8>() {
				Ok(i) => {
					if i == 1{
						con = false;
						question_data.push(i);
					} else if i == 0{
						con = false;
						question_data.push(i);
					} else {
						println!("Please input a 1 or 0");
					}

				}
				Err(..) => {
					println!("Please input an integer. 1 for yes. 0 for no");
				}
			}
		}

	}

	question_data
}

fn read_happines_and_sadness(questions: Vec<&str>) -> Vec<u8>{
	let mut question_data: Vec<u8>= Vec::new();

	for word in questions{

		let mut con = true;

		while con{
			println!("Would you say you felt {} today? ", word);
			let mut input_line = String::new();
			io::stdin()
			.read_line(&mut input_line)
			.expect("Failed to read input");

			let trimmed_input = input_line.trim();

			match trimmed_input.parse::<u8>() {
				Ok(i) => {
					if i >= 1 && i <= 5 {
						con = false;
						question_data.push(i);
					} else {
						println!("Please input an integer between 1 and 5");
					}
				}
				Err(..) => {
					println!("Please input an integer");
				}
			}
		}

	}

	question_data
}

fn write_number_score_to_file(path: &std::path::PathBuf, data:Vec<u8> ){
	let mut file = OpenOptions::new()
	.read(true)
	.write(true)
	.append(true)
	.create(true)
	.open(&path)
	.unwrap();

	let date = Local::now();
	let mut write_string = String::from(date.format("%Y:%m:%d").to_string() + ",");

	for score in data {
		write_string += &(score.to_string() + ",");
	}

	file.write((write_string +"\n").as_bytes())
	.expect("Failed to write to file");
}

fn main() {

	let intro_string = String::from("A number of question will be asked. It will be divided into three parts.
The first is PANAS questions. There you will anser with a number between 1 and 5
The second part is question about wether or not you felt happy. The third part
asks you a couple you if you  done a cople of thing during the day. The idea
behind this is for you to be able to see what makes you more happy. All your
answers will be saved to your computer. If you want to see graphs of your answers
please download the analytics tool written i python (link).
-----------------------------------
");

	println!("{}", intro_string);

	let words = vec!["Interested","Distressed","Excited",
					"Upset","Strong","Guilty","Scared",
					"Hostile","Enthusiastic","Proud",
					"Irritable","Alert","Ashamed",
					"Inspired","nervous","Determined",
					"Attentive","Jittery","Active","Afraid"];

	let positive = vec!["Interested","Excited","Strong",
						"Enthusiastic","Proud","Alert",
						"Inspired","Determined","Attentive"];

	let negative = vec!["Distressed","Upset","Guilty",
						"Scared","Hostile","Irritable",
						"Ashamed","Nervous","Jittery",
						"Active","Afraid"];

	let args = Cli::from_args();

	println!("\n--------------------\nPart 1. PANAS QUESTIONS \n--------------------\n");

	let mut data = read_happiness(words);

	let pos_score = score_calculator(positive, &data);
	let neg_score = score_calculator(negative, &data);

	data.push(Feeling{
		name: String::from("Positve_score"),
		value:pos_score});
	data.push(Feeling{

		name: String::from("negative_score"),
		value:neg_score});

	if args.show_scores{
		println!("Your positive score was {}", pos_score);
		println!("Your negative score was {}", neg_score);
	}



	let questions = vec!["walk 10 000 steps","walk 15 000 steps",
							"hangout with friends","go for a run"];

	println!("--------------------\nNow begins part two of the questions. These will be yes or no question.\n1 - yes\n0 - no \n--------------------\n");


	let life_question_data = read_life_question(questions);


//	let life_question_path = PathBuf::from((&args.path).to_owned() + "_life_questions.txt");


	println!("--------------------\\nNow begins the third part. This part only contains 2 questions. The answer should be on a scale between 1 and 5. Where 1 is 'no, I have no felt it' and 5 is 'yes, I definitely felt it'\n--------------------\n");

	let happines_sad_data = read_happines_and_sadness(vec!["Happy","Sad"]);

//	let questions_path = PathBuf::from(&(args.path + "_simple_questions.txt"));


//PART WHERE ALL THE DATA IS SAVED
	fs::create_dir("./data_happiness");

	let panas_path = PathBuf::from("./data_happiness/data_PANAS.txt");
	write_values_to_file(&panas_path, &data);

	let life_question_path = PathBuf::from("./data_happiness/data_life_questions.txt");
	write_number_score_to_file(&life_question_path,life_question_data);

	let questions_path = PathBuf::from("./data_happiness/data_simple_questions.txt");
	write_number_score_to_file(&questions_path,happines_sad_data);
}
